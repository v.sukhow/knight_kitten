package knight;

import java.util.Scanner;
import java.util.Vector;

public class KnightSteps {

    static int countSteps;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextLong()) {
            countSteps = 0;
            int nx = sc.nextInt();
            int ny = sc.nextInt();

            int knightX = sc.nextInt();
            int knightY = sc.nextInt();

            int targetX = sc.nextInt();
            int targetY = sc.nextInt();

            long startTime = System.currentTimeMillis();
            getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);

            //uncomment to get how long method works in milliseconds
            //System.out.println(String.format("Method works %s milliseconds.", System.currentTimeMillis() - startTime));
        }
    }

    static void getMinStepsToReachTarget(int knightX, int knightY, int targetX, int targetY, int nx, int ny) {
        //cut of Knight position to (0,0) and target position to 1 quater (targetX and targetY will be >= knightX and knightY)
        targetX = Math.abs(targetX - knightX);
        targetY = Math.abs(targetY - knightY);

        knightX = 0;
        knightY = 0;

        if (nx - targetX > 10) {
            nx = targetX + 6;
        }
        if (ny - targetY > 10) {
            ny = targetY + 6;
        }

        if (Math.abs(knightX - targetX) < 1000 && Math.abs(knightY - targetY) < 1000) {
            //method works good with small knight and target values
            System.out.println(countSteps + minStepToReachTarget(knightX, knightY, targetX, targetY, nx, ny));

        } else {
            //cut of and move knight to right direction
            cutOfAndGetMinStepToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
        }
    }


    static void cutOfAndGetMinStepToReachTarget(int knightX, int knightY, int targetX, int targetY, int nx, int ny) {
        if (targetX == targetY) {
            //two steps of the knight --> 1. x+2, y+1 2. x+1, y+2
            int i = (targetX - 10) / 3;

            countSteps += i * 2;
            knightX += i * 3;
            knightY += i * 3;

            getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
        } else {
            if (targetX > targetY) {

                if (targetY == 0) {
                    //move only by X
                    int i = (targetX - 10) / 2;
                    countSteps += i;
                    knightX += i * 2;
                    if (knightX % 4 != 0) {
                        knightY += 1;
                    }
                    getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);

                } else {
                    int difference = targetX / targetY;

                    // check if targetX more then 2 times than targetY
                    if (difference > 2) {
                        // that move knight on count steps == targetY
                        knightY += targetY;
                        knightX += targetY * 2;
                        countSteps += targetY;

                        getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
                    } else {
                        difference = targetX - targetY;
                        //move knight on difference of

                        knightX += difference * 2;
                        knightY += difference;
                        countSteps += difference;

                        getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
                    }
                }

            } else {
                if (targetX == 0) {
                    //move only by Y
                    int i = (targetY - 10) / 2;
                    countSteps += i;
                    knightY += i * 2;
                    if (knightY % 4 != 0) {
                        knightX += 1;
                    }
                    getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
                } else {
                    int difference = targetY / targetX;
                    // check if targetY more then 2 times than targetX
                    if (difference > 2) {
                        // that move knight on count steps == targetX
                        knightX += targetX;
                        knightY += targetX * 2;
                        countSteps += targetX;

                        getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
                    } else {
                        difference = targetY - targetX;
                        //move knight on difference of

                        knightY += difference * 2;
                        knightX += difference;
                        countSteps += difference;

                        getMinStepsToReachTarget(knightX, knightY, targetX, targetY, nx, ny);
                    }
                }
            }
        }
    }

    static int minStepToReachTarget(int knightX, int knightY, int targetX, int targetY, int sizeX, int sizeY) {
        knightX += 1;
        knightY += 1;
        targetX += 1;
        targetY += 1;

        int[] knightPos = {knightX, knightY};
        int[] targetPos = {targetX, targetY};

        // x and y direction, where a knight can move
        int[] dx = {-2, -1, 1, 2, -2, -1, 1, 2};
        int[] dy = {-1, -2, -2, -1, 1, 2, 2, 1};

        // queue for storing states of knight in board
        Vector<Cell> q = new Vector<>();

        // push starting position of knight with 0 distance
        q.add(new Cell(knightPos[0], knightPos[1], 0));

        Cell t;
        int x, y;
        boolean[][] visit = new boolean[sizeX + 1][sizeY + 1];

        // make all cell unvisited
        for (int i = 1; i <= sizeX; i++)
            for (int j = 1; j <= sizeY; j++)
                visit[i][j] = false;

        // visit starting state
        visit[knightPos[0]][knightPos[1]] = true;

        // loop untill we have one element in queue
        while (!q.isEmpty()) {
            t = q.firstElement();
            q.remove(0);

            // if current cell is equal to target cell,
            // return its distance
            if (t.x == targetPos[0] && t.y == targetPos[1]) {
                return t.dis;
            }

            // loop for all reachable states
            for (int i = 0; i < 8; i++) {
                x = t.x + dx[i];
                y = t.y + dy[i];

                // If reachable state is not yet visited and
                // inside board, push that state into queue
                if (isInside(x, y, sizeX, sizeY) && !visit[x][y]) {
                    visit[x][y] = true;
                    q.add(new Cell(x, y, t.dis + 1));
                }
            }
        }
        return Integer.MAX_VALUE;
    }

    static class Cell {
        int x, y;
        int dis;

        public Cell(int x, int y, int dis) {
            this.x = x;
            this.y = y;
            this.dis = dis;
        }
    }

    static boolean isInside(long x, long y, long Nx, long Ny) {
        return x >= 1 && x <= Nx && y >= 1 && y <= Ny;
    }

}