package kitten;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ColorGuide {


    public String[][] generateTablePossibleGenes(List<String> maleGeneVariants,
                                                 List<String> femaleGeneVariants) {
        int amountColumns = femaleGeneVariants.size() + 1;
        int amountRows = maleGeneVariants.size() + 1;

        String[][] tableGenes = new String[amountRows][amountColumns];

        for (int i = 1; i < amountColumns; i++) {
            tableGenes[0][i] = femaleGeneVariants.get(i - 1);
        }

        for (int i = 1; i < amountRows; i++) {
            tableGenes[i][0] = maleGeneVariants.get(i - 1);
        }

        for(int i=1;i< amountRows; i++) {
            for (int j = 1; j < amountColumns; j++ ){
                tableGenes[i][j] = unionTwoColorCodes(tableGenes[i][0], tableGenes[0][j]);
            }
        }
        return tableGenes;
    }


    //example result [db, dB, Db, DB]
    private List<String> getAllPairsWithTwoChars(char firstChar, char secondChar) {
        char[] variantsFirstChar = getCharArrayWithBigSmallChars(firstChar);
        char[] variantsSecondChar = getCharArrayWithBigSmallChars(secondChar);

        List<String> allPairs = new ArrayList<>(4);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                allPairs.add(generateStringWithTwoChars(variantsFirstChar[i], variantsSecondChar[j]));
            }
        }
        return allPairs;
    }

    //example result [B,b]
    private char[] getCharArrayWithBigSmallChars(char letter) {
        char[] array = new char[2];

        if (Character.isUpperCase(letter)) {
            array[0] = Character.toLowerCase(letter);
            array[1] = letter;
        } else {
            array[0] = letter;
            array[1] = Character.toUpperCase(letter);
        }
        return array;
    }

    private String generateStringWithTwoChars(char firstChar, char secondChar) {
        char[] array = {firstChar, secondChar};
        return String.valueOf(array);
    }

    // pass RED code male/female
    //get [bdO, bDO, BdO, BDO]
    private List<String> getCombinationsDominantRedGene(String colorCode) {
        List<String> possibleGeneCombs = new ArrayList<>();
        char[] colorCodeChars = colorCode.toCharArray();
        int sizeCode = colorCode.length();
        boolean isMale = colorCode.length() < 4;
        possibleGeneCombs = getAllPairsWithTwoChars('b', colorCodeChars[0]);
        possibleGeneCombs = possibleGeneCombs.stream().map(comb -> comb + 'O').collect(Collectors.toList());
        if (isMale) {
            possibleGeneCombs.addAll(getAllPairsWithTwoChars('b', colorCodeChars[0]));
        }
        return possibleGeneCombs;
    }

    private List<String> getCombinationsTorties(String colorCode, List<Character> colorCodeChars) {
        List<String> possibleGeneCombs = new ArrayList<>();
        int sizeCode = colorCode.length();

        List<Integer> positionsDashes = getPositionsOfDashesAtGenes(colorCodeChars);

        if (positionsDashes.isEmpty()) {
            String lettersGenes = colorCode.substring(1, 3);
            possibleGeneCombs.add(lettersGenes + colorCode.charAt(sizeCode - 2));
            possibleGeneCombs.add(lettersGenes + colorCode.charAt(sizeCode - 1));
        } else if (positionsDashes.size() == 1) {
            char[] bigSmallRed = getCharArrayWithBigSmallChars(colorCode.charAt(sizeCode - 1));
            possibleGeneCombs = createCombsWithOneDash(possibleGeneCombs, colorCode,
                    false, positionsDashes.get(0), bigSmallRed);
        } else {
            char[] bigSmallRed = getCharArrayWithBigSmallChars(colorCode.charAt(sizeCode - 1));
            possibleGeneCombs = createCombsWithTwoDashes(possibleGeneCombs, colorCode,
                    false, positionsDashes, bigSmallRed);
        }
        return possibleGeneCombs;
    }

    private List<String> getCombinationsNoDominantRed(String colorCode, List<Character> colorCodeChars, boolean isMale) {
        List<String> possibleGeneCombs = new ArrayList<>();
        List<Integer> positionsDashes = getPositionsOfDashesAtGenes(colorCodeChars);
        if (positionsDashes.size() == 0) {
            String letterGenes = colorCode.substring(1, 3);
            possibleGeneCombs.add(letterGenes + 'o');
            if (isMale) {
                possibleGeneCombs.add(letterGenes);
            }
        } else if (positionsDashes.size() == 1) {
            possibleGeneCombs =
                    createCombsWithOneDash(possibleGeneCombs, colorCode, isMale, positionsDashes.get(0), new char[]{'o'});
        } else {
            possibleGeneCombs =
                    createCombsWithTwoDashes(possibleGeneCombs, colorCode, isMale, positionsDashes, new char[]{'o'});
        }
        return possibleGeneCombs;
    }

    private List<String> createCombsWithOneDash(List<String> possibleGeneCombs, String colorCode, boolean isMale,
                                                int dashPosition, char[] charsForRed) {
        char[] newComb = new char[3];
        int arrayIndex = dashPosition < 2 ? 1 : 0;
        newComb[arrayIndex] = dashPosition < 2 ? colorCode.charAt(dashPosition + 1) : colorCode.charAt(0);
        char[] bigLittleCharAtDash = getCharArrayWithBigSmallChars(colorCode.charAt(dashPosition - 1));

        if (charsForRed.length == 2) { //for Torties
            for (int i = 0; i < bigLittleCharAtDash.length; i++) {
                int index = dashPosition < 2 ? 0 : 1;
                newComb[index] = bigLittleCharAtDash[i];
                IntStream.range(0, 2).forEach((j) -> {
                    newComb[2] = charsForRed[j];
                    possibleGeneCombs.add(String.valueOf(newComb));
                });
            }
        } else {
            int index = dashPosition < 2 ? 0 : 1;
            IntStream.range(0, 2).forEach(i -> {
                newComb[index] = bigLittleCharAtDash[i];
                newComb[2] = charsForRed[0];
                possibleGeneCombs.add(String.valueOf(newComb));
            });
            if (isMale) {
                IntStream.range(0, 2).forEach(i -> {
                    newComb[index] = bigLittleCharAtDash[i];
                    possibleGeneCombs.add(String.valueOf(newComb).substring(0, 2));
                });
            }
        }
        return possibleGeneCombs;
    }

    private List<String> createCombsWithTwoDashes(List<String> possibleGeneCombs, String colorCode, boolean isMale,
                                                  List<Integer> positionsDashes, char[] charsForRed) {
        char charFirstGene = colorCode.charAt(positionsDashes.get(0) - 1);
        char charSecondGene = colorCode.charAt(positionsDashes.get(1) - 1);

        List<String> geneCombsWithoutRed = getAllPairsWithTwoChars(charFirstGene, charSecondGene);
        if (charsForRed.length == 2) { //for Torties
            IntStream.range(0, 2).forEach(i -> {
                char red = charsForRed[i];
                IntStream.range(0, 4).forEach(j -> {
                    possibleGeneCombs.add(geneCombsWithoutRed.get(j) + red);
                });
            });
        } else {
            char red = charsForRed[0];
            IntStream.range(0, geneCombsWithoutRed.size()).forEach(i -> {
                possibleGeneCombs.add(geneCombsWithoutRed.get(i) + red);
            });

            if (isMale) {
                IntStream.range(0, geneCombsWithoutRed.size()).forEach(i -> {
                    possibleGeneCombs.add(geneCombsWithoutRed.get(i));
                });
            }
        }
        return possibleGeneCombs;
    }


    public List<String> getAllGenesCombsByColorAndGender(String color, boolean isMale) {
        List<String> possibleGeneCombs = new ArrayList<>();
        String[] colorCodeAndTypeCat = getColorCodeAndTypeCat(color, isMale);
        String colorCode = colorCodeAndTypeCat[0];
        String catType = colorCodeAndTypeCat[1];

        int sizeCode = colorCode.length();

        List<Character> colorCodeChars = new ArrayList<>();
        for (int i = 0; i < sizeCode; i++) {
            colorCodeChars.add(colorCode.charAt(i));
        }

        if (catType.equals("DominantRedGene")) {
            possibleGeneCombs = getCombinationsDominantRedGene(colorCode);
        } else if (catType.equals("Torties")) {
            possibleGeneCombs = getCombinationsTorties(colorCode, colorCodeChars);
        } else {
            possibleGeneCombs = getCombinationsNoDominantRed(colorCode, colorCodeChars, isMale);
        }
        return possibleGeneCombs;
    }


    private List<Integer> getPositionsOfDashesAtGenes(List<Character> colorCodeChars) {
        List<Integer> amountDishesList = new ArrayList<>();
        //search amount dashes
        // length-2 since last 2  symbols doesn't interest us
        for (int i = 0; i < colorCodeChars.size(); i++) {
            if (colorCodeChars.get(i) == '-') {
                amountDishesList.add(i);
            }
        }
        return amountDishesList;
    }

    private String[] getColorCodeAndTypeCat(String color, boolean isMale) {
        String[] colorCodeAndTypeCat = new String[2];
        if (Kitten.colorsNoDominantRedGene.containsKey(color)) {
            colorCodeAndTypeCat[0] = Kitten.colorsNoDominantRedGene.get(color);
            colorCodeAndTypeCat[1] = "NoDominantRed";
        } else if (Kitten.colorsTorties.containsKey(color)) {
            colorCodeAndTypeCat[0] = Kitten.colorsTorties.get(color);
            colorCodeAndTypeCat[1] = "Torties";
        } else {
            colorCodeAndTypeCat[0] = isMale ?
                    Kitten.colorsMaleDominantRedGene.get(color) :
                    Kitten.colorsFemaleDominantRedGene.get(color);
            colorCodeAndTypeCat[1] = "DominantRedGene";
        }
        return colorCodeAndTypeCat;
    }

    public String unionTwoColorCodes(String maleCode, String femaleCode) {
        char[] femaleCodeChars = femaleCode.toCharArray();
        char[] maleCodeChars = maleCode.toCharArray();

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < 2; i++) {
            result.append(femaleCodeChars[i]);
            result.append(maleCodeChars[i]);
            result = (i == 0) ? swapCharsbyCase(result, i, i + 1) :
                    swapCharsbyCase(result, i + 1, i + 2);
        }

        result.append(femaleCodeChars[2]);
        if (maleCode.length() == femaleCodeChars.length) {
            result.append(maleCodeChars[2]);
            result = swapCharsbyCase(result, 4, 5);
        }
        return result.toString();
    }


    public StringBuilder swapCharsbyCase(StringBuilder stringBuilder, int firstIndex, int secondIndex) {
        char firstChar = stringBuilder.charAt(firstIndex);
        char secondChar = stringBuilder.charAt(secondIndex);

        if (Character.isUpperCase(firstChar) && Character.isLowerCase(secondChar)
                || Character.isLowerCase(firstChar) && Character.isLowerCase(secondChar)) {
            return stringBuilder;
        } else {
            char buffer = firstChar;
            stringBuilder.setCharAt(firstIndex, secondChar);
            stringBuilder.setCharAt(secondIndex, buffer);
            return stringBuilder;
        }
    }

    public String getColorByCode(String colorCode) {
        int lengthCode = colorCode.length();
        if (colorCode.substring(lengthCode - 2).equals("OO")
                || colorCode.substring(lengthCode - 1).equals("O")) { //Red girl
            return getRedShade(colorCode);
        } else if (colorCode.substring(lengthCode - 2).equals("Oo")) {
            return getBlackShade(colorCode);
        }
        else {
            return getPureColor(colorCode);
        }
    }

    public String getPureColor(String colorCode) {
        if (colorCode.contains("B") && colorCode.contains("D") )
            return "Black";
        else if (colorCode.contains("B"))
            return "Blue";
        else if(colorCode.contains("D"))
            return "Chocolate";
        else return "Lilac";

    }

    public String getRedShade(String colorCode) {
        return colorCode.contains("D")? "Red" : "Cream";
    }

    public String getBlackShade(String colorCode) {
        if (colorCode.contains("B") && colorCode.contains("D") )
            return "Black-Red";
        else if (colorCode.contains("B"))
            return "Blue-Cream";
        else if(colorCode.contains("D"))
            return "Chocolate-Red";
        else return "Lilac-Cream";
    }

    public Map<String, Integer> getAllKittenColors(String[][] table) {
        Map<String, Integer> amountColors = new HashMap<>() {{
            put("Red", 0);
            put("Cream", 0);
            put("Black-Red", 0);
            put("Blue-Cream", 0);
            put("Chocolate-Red", 0);
            put("Lilac-Cream", 0);
            put("Black", 0);
            put("Blue", 0);
            put("Chocolate", 0);
            put("Lilac", 0);
        }};


        for (int i=1; i < table.length; i++) {
            for (int j=1; j< table[0].length;j++) {
                switch (getColorByCode(table[i][j])) {
                    case "Red": {
                        increaseValueByKey("Red", amountColors);
                        break;
                    }
                    case "Cream": {
                        increaseValueByKey("Cream", amountColors);
                        break;
                    }
                    case "Black-Red" : {
                        increaseValueByKey("Black-Red", amountColors);
                        break;
                    }
                    case "Blue-Cream" : {
                        increaseValueByKey("Blue-Cream", amountColors);
                        break;
                    }
                    case "Chocolate-Red" : {
                        increaseValueByKey("Chocolate-Red", amountColors);
                        break;
                    }
                    case "Lilac-Cream" : {
                        increaseValueByKey("Lilac-Cream", amountColors);
                        break;
                    }
                    case "Black" : {
                        increaseValueByKey("Black", amountColors);
                        break;
                    }
                    case "Blue" : {
                        increaseValueByKey("Blue", amountColors);
                        break;
                    }
                    case "Chocolate" : {
                        increaseValueByKey("Chocolate", amountColors);
                        break;
                    }
                    default: {
                        increaseValueByKey("Lilac", amountColors);
                        break;
                    }

                }
            }
        }
        return amountColors;
    }

    private void increaseValueByKey(String color,  Map<String, Integer> amountColors) {
        int tmp = Integer.valueOf(amountColors.get(color).intValue() + 1);
        amountColors.put(color, tmp);
    }

    public int getAmountAllCombs (String[][] tableCombs) {
        return (tableCombs.length-1) * (tableCombs[0].length - 1);
    }

    public void showProbablyKittenColors(Map<String, Integer> colorsMap, int totalAmountCombs) {
        Map<String, Double> colorProbabilities = new HashMap<>();
        int amountAllCombs = totalAmountCombs;
        for (Map.Entry entry : colorsMap.entrySet()) {
            if(!(entry.getValue() == Integer.valueOf(0))) {
                Integer value = (Integer) entry.getValue();
                double result =(double)value/amountAllCombs;
                colorProbabilities.put((String) entry.getKey(), result);
            }
        }

        for (Map.Entry entry : colorProbabilities.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}




