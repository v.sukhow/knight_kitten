package kitten;

import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class OffspringColorProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {

            String femaleColor = sc.nextLine();
            String maleColor = sc.nextLine();

            ColorGuide colorGuide = new ColorGuide();

            List<String> allGenesCombsDominantRedMale = colorGuide.getAllGenesCombsByColorAndGender(maleColor, true);
            List<String> allGenesCombsDominantRedFemale = colorGuide.getAllGenesCombsByColorAndGender(femaleColor, false);

            String[][] tableKittens = colorGuide.generateTablePossibleGenes(allGenesCombsDominantRedMale, allGenesCombsDominantRedFemale);

            //uncomment it to see the table of possible future kittens genes combinations
            //for example let's take Red Red,
            //you will get 32 possible coombination
            //and 8 of them will be with dd0 or dd00 (Cream color), other Red (Red : 0.75 Cream : 0.25)
            //but in your answer Red 0.937500000 Cream 0.062500000. I'm stuck here a lot

            /*Arrays.stream(tableKittens).forEach(tableKitten -> {
                IntStream.range(0, tableKitten.length).forEach(j -> {
                    if (tableKitten[j] == null) {
                        System.out.print("Male\\Female");
                    } else {
                        System.out.print(tableKitten[j]);
                    }
                    if (j < tableKitten.length - 1) System.out.print("                    ");
                });
                System.out.println();
            });*/

            Map<String, Integer> colorsMap = colorGuide.getAllKittenColors(tableKittens);
            int amountColors = colorGuide.getAmountAllCombs(tableKittens);
            colorGuide.showProbablyKittenColors(colorsMap, amountColors);
        }
    }
}
