package kitten;

import java.util.HashMap;
import java.util.Map;

public class Kitten {
    //male has o
    //female has oo
    public static Map<String, String> colorsNoDominantRedGene = new HashMap<>() {{
        put ("Black","B-D-");
        put ("Blue","B-dd");
        put ("Chocolate","bbD-");
        put("Lilac","bbdd");
    }};

    public static Map<String, String> colorsFemaleDominantRedGene = new HashMap<>() {{
        put("Red", "D-OO");
        put("Cream", "ddOO");
    }};

    public static Map<String, String> colorsMaleDominantRedGene = new HashMap<>() {{
        put("Red", "D-O");
        put("Cream", "ddO");
    }};

    //tortie is female and has Oo
    public static Map<String, String> colorsTorties = new HashMap<>() {{
        put("Black-Red Tortie", "B-D-Oo");
        put("Blue-Cream Tortie", "B-ddOo");
        put("Chocolate-Red Tortie", "bbD-Oo");
        put("Lilac-Cream Tortie", "bbddOo");
    }};

}
